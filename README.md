# package-lists

Packages I like to install on various Linux distros.

These are intended for use with [aptorphan/pacorphan](https://delx.net.au/git/scripts/blob_plain/HEAD:/pacorphan) from my [scripts](https://delx.net.au/git/scripts) repository.
